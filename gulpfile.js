var gulp = require('gulp');
var mjmlEngine = require('mjml').default
var mjml = require('gulp-mjml');
var browserSync = require('browser-sync').create();

gulp.task('html', function(){
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: true}, {beautify: false}))
        .pipe(gulp.dest('./dist'))
})

gulp.task('html-beautify', function(){
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: false}, {beautify: true}))
        .pipe(gulp.dest('./dist'))
})

gulp.task('serve', function() {

    browserSync.init({
        server: "./dist"
    });

    gulp.watch("./dist/*.html").on('change', browserSync.reload);
});

gulp.task('watch', ['serve'], function() {
    gulp.watch('src/*.mjml', ['html']);
});

gulp.task('default', ['watch']);
